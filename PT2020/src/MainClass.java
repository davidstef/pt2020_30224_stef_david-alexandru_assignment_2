import java.io.*;
import java.util.ArrayList;

public class MainClass {
	/*
	 * 1. nr de clienti 2. nr de cozi 3. timpul total de stat 4. interval la care
	 * poate sosi un client [x1, x2] 5. intervalul cat sta [x1, x2]
	 */

	public static void main(String[] args) throws IOException {
		FileInputStream f = new FileInputStream(args[0]);
		InputStreamReader fchar = new InputStreamReader(f);
		BufferedReader buff = new BufferedReader(fchar);
		int nrCl = Integer.parseInt(buff.readLine());//
		int nrCozi = Integer.parseInt(buff.readLine());//
		int timpT = Integer.parseInt(buff.readLine());//
		String str1 = buff.readLine();//
		String[] s = str1.split(",");
		int sos1 = Integer.parseInt(s[0]);
		int sos2 = Integer.parseInt(s[1]);
		str1 = buff.readLine();//
		s = str1.split(",");
		int ast1 = Integer.parseInt(s[0]);
		int ast2 = Integer.parseInt(s[1]);
		String file = new String(args[1]);
		ArrayList<Client> clienti = new ArrayList<Client>();
		SimulareManager simulare = new SimulareManager(file, nrCl, nrCozi, timpT, sos1, sos2, ast1, ast2, clienti);
		clienti = simulare.randomClienti();
		simulare = new SimulareManager(file, nrCl, nrCozi, timpT, sos1, sos2, ast1, ast2, clienti);
		Thread process = new Thread(simulare);
		process.start();
	}
}
