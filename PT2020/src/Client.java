import java.util.Comparator;

/*
	 * 1. nr de clienti
	 * 2. nr de cozi
	 * 3. timpul total de stat
	 * 4. interval la care poate sosi un client [x1, x2]
	 * 5. intervalul cat sta [x1, x2]
	 */
public class Client {
	private int nrClient;
	private int timpSosire;
	private int timpAsteptare;
	
	public Client(int nrClient, int timpSosire, int timpAsteptare) {
		super();
		this.nrClient = nrClient;
		this.timpSosire = timpSosire;
		this.timpAsteptare = timpAsteptare;
	}
	public int getNrClient() {
		return nrClient;
	}
	public void setNrClient(int nrClient) {
		this.nrClient = nrClient;
	}
	public int getTimpSosire() {
		return timpSosire;
	}
	public void setTimpSosire(int timpSosire) {
		this.timpSosire = timpSosire;
	}
	public int getTimpAsteptare() {
		return timpAsteptare;
	}
	public void setTimpAsteptare(int timpAsteptare) {
		this.timpAsteptare = timpAsteptare;
	}
	@Override
	public String toString() {
		return "nrCl=" + nrClient + ", tSos=" + timpSosire + ", tAst=" + timpAsteptare + " | ";
	}
}

class ComparaClienti implements Comparator<Client>{
	/// vom folosi pentru a memora clientii crescator dupa numarul timpul de sosire
	@Override
	public int compare(Client c1, Client c2) {
		Integer timpSos = new Integer(c2.getTimpSosire());
		Integer i = new Integer(c1.getTimpSosire()).compareTo(timpSos);
		return i;
	}
}
