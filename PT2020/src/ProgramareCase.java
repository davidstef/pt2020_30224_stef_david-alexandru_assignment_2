import java.util.ArrayList;

public class ProgramareCase {
	public int timpTotal;
	public ArrayList<Coada> listaCase;
	public int numarCase;

	public ProgramareCase(int timpTotal, int numarCase) {
		super();
		listaCase = new ArrayList<Coada>();
		for (int i = numarCase - 1; i >= 0; i--) {
			Coada casa = new Coada(i);
			listaCase.add(casa);
			Thread t = new Thread(casa);
			t.start();
		}
		this.numarCase = numarCase;
		this.timpTotal = timpTotal;
	}

	public int getTimpServireTotal() {
		int timp = 0;
		for (int j = 0; j < listaCase.size(); j++) {
			timp = timp + listaCase.get(j).getTimpAst();
		}
		return timp;
	}

	public Coada timpMinimCoada() {
		Coada auxCasa = new Coada(), casaDeschisa = new Coada();
		int min = 999999;
		boolean casaFull = true, caseGoale = false;
		for (int i = 0; i < listaCase.size(); i++) {
			if (listaCase.get(i).isOpen() == false) {
				auxCasa = listaCase.get(i);
				caseGoale = true;
			} else {
				casaFull = (listaCase.get(i).getNumarClienti() > 0) ? true : false;
				if (min > listaCase.get(i).getTimpAst()) {
					casaDeschisa = listaCase.get(i);
					min = listaCase.get(i).getTimpAst();
				}
			}
		}
		if (casaFull == true && caseGoale == true) {
			//System.out.println("Coada " + (casaDeschisa.getNumarCoada() + 1) + " se deschide.");
			casaDeschisa = auxCasa;
		}
		return casaDeschisa;
	}

	public void ordonareClienti(Client cl, Coada casa) {
		casa = timpMinimCoada();
		casa.setOpen(true);
		casa.adaugaClient(cl);
		System.out.println("Clientul " + (cl.getNrClient()+1) + " merge la coada " + (casa.getNumarCoada() + 1));
		// System.out.println("Queue " + (casa.getNumarCoada() + 1) + ": " + "(" +
		// cl.getNrClient() + ", "
		// + cl.getTimpSosire() + ", " + cl.getTimpAsteptare() + ")");
	}

	public double getTimpMediu() {
		double medie = 0.0;
		int nrCl = 0;
		for (int i = 0; i < listaCase.size(); i++) {
			if (listaCase.get(i).getNumarClienti() != 0) {
				medie = medie + listaCase.get(i).getTimp();
			}
			nrCl += listaCase.get(i).getNumarClienti();
		}
		return medie / nrCl;
	}

	public Coada getCasaGoala() {
		for (int i = 0; i < listaCase.size(); i++) {
			if (listaCase.get(i).isOcupata() == false)
				return listaCase.get(i);
		}
		return null;
	}

}
