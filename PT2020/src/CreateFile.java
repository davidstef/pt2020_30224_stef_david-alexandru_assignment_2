import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class CreateFile {
	private FileWriter file;

	public CreateFile(String file) {
		super();
		try {
			this.file = new FileWriter(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void printTimpMediu(double t) {
		String strData = new String();
		strData = "Average Waiting Time: " + t + "\n";
		try {
			file.write(strData);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public FileWriter getFile() {
		return file;
	}

	public void setFile(FileWriter file) {
		this.file = file;
	}

	public void printWaitingList(int time, ArrayList<Client> clienti, ProgramareCase cozi) {
		String stringData = new String();
		stringData = stringData + "Time " + time + ":\n";

		stringData = stringData + "Waiting Clients: ";
		for (int i = 0; i < clienti.size(); i++) {
			stringData = stringData + "(" + (clienti.get(i).getNrClient() + 1) + ", " + clienti.get(i).getTimpSosire()
					+ ", " + clienti.get(i).getTimpAsteptare() + "); ";
		}
		stringData += "\n";
		for (int i = 0; i < cozi.listaCase.size(); i++) {
			boolean open = false;
			stringData += "Queue " + (cozi.listaCase.get(i).getNumarCoada() + 1) + ": ";
			for (Client client : cozi.listaCase.get(i).getClienti()) {
				open = true;
				stringData += "(" + (client.getNrClient() + 1) + ", " + client.getTimpSosire() + ", "
						+ client.getTimpAsteptare() + "); ";
			}
			if (false == open) {
				stringData += "Closed;";
			}
			stringData += "\n";
		}
		stringData += "\n";

		try {
			file.write(stringData);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
