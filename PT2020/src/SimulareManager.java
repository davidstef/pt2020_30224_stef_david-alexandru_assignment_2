import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class SimulareManager implements Runnable {
	private int numarClienti;
	private int nrCozi;
	private int timpTotal;
	private int timpSosireMin;
	private int timpSosireMax;
	private int timpAsteptareMin;
	private int timpAsteptareMax;
	private volatile ArrayList<Client> clienti;
	private int time;
	private ProgramareCase programare;
	private CreateFile file; 
	
	public SimulareManager(String file, int nrClienti, int nrCozi, int timpTotal, int timpSosire1, int timpSosire2, int timpAsteptare1,
			int timpAsteptare2, ArrayList<Client> clienti) {
		super();
		this.file = new CreateFile(file);
		this.numarClienti = nrClienti;
		this.nrCozi = nrCozi;
		this.timpTotal = timpTotal;
		this.timpSosireMin = timpSosire1;
		this.timpSosireMax = timpSosire2;
		this.timpAsteptareMin = timpAsteptare1;
		this.timpAsteptareMax = timpAsteptare2;
		this.clienti = clienti;
		this.time = 0;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}
	
	public ArrayList<Client> randomClienti() {
		int timpSos = 0;
		ArrayList<Client> clienti = new ArrayList<Client>();
		int i = 0;
		while (i < numarClienti) {
			Random rand = new Random();
			int asteptare = (timpAsteptareMin + timpSos)
					+ rand.nextInt((timpAsteptareMax + timpSos) - (timpAsteptareMin + timpSos) + 1);
			int sosire = timpSosireMin + rand.nextInt(timpSosireMax - timpSosireMin + 1);
			timpSos = sosire;
			Client c = new Client(i, sosire, asteptare);
			clienti.add(c);
			i++;
		}
		Collections.sort(clienti, new ComparaClienti());
		return clienti;
	}
	
	@Override
	public void run() {
		programare = new ProgramareCase(timpTotal, nrCozi);
		int max = -1;
		for (time = 0; time < timpTotal; time++) {
			ArrayList<Client> auxC = new ArrayList<Client>();
			for (int i = 0; i < clienti.size(); i++) {
				if (clienti.get(i).getTimpSosire() == time) {
					auxC.add(clienti.get(i));
					programare.ordonareClienti(clienti.get(i), programare.getCasaGoala());
				}
			}
			clienti.removeAll(auxC);
			file.printWaitingList(this.time, clienti, programare);
			if (max < programare.getTimpServireTotal()) {
				max = programare.getTimpServireTotal();
			}
			System.out.println("Time: " + time);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		file.printTimpMediu(programare.getTimpMediu());
		try {
			file.getFile().close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Timp mediu: " + programare.getTimpMediu());
	}
}
