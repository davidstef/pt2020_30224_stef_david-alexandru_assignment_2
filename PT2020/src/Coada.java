import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Coada implements Runnable {
	private volatile BlockingQueue<Client> coadaClienti;
	private volatile int numarCoada;
	private volatile boolean isOpen;
	private volatile int numarClienti;
	private volatile int timpAst;
	private volatile double timpMediu;
	private volatile boolean ocupata;

	public Coada(int nrCoada) {
		this.coadaClienti = new ArrayBlockingQueue<Client>(100);
		this.numarCoada = nrCoada;
		this.isOpen = false;
		this.numarClienti = 0;
		this.timpAst = 0;
		this.timpMediu = 0.0;
		this.ocupata = false;
	}

	public Coada() {
		// TODO Auto-generated constructor stub
	}

	public boolean isOcupata() {
		return ocupata;
	}

	public void setOcupata(boolean ocupata) {
		this.ocupata = ocupata;
	}

	public int getNumarCoada() {
		return numarCoada;
	}

	public void setNumarCoada(int numarCoada) {
		this.numarCoada = numarCoada;
	}

	public boolean isOpen() {
		return isOpen;
	}

	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}

	public int getNumarClienti() {
		return numarClienti;
	}

	public void setNumarClienti(int numarClienti) {
		this.numarClienti = numarClienti;
	}

	public int getTimpAst() {
		return timpAst;
	}

	public void setTimpAst(int timpAst) {
		this.timpAst = timpAst;
	}

	public Client getClient() {
		return coadaClienti.peek();
	}

	public BlockingQueue<Client> getClienti() {
		return this.coadaClienti;
	}

	public double getTimp() {
		return this.timpMediu;
	}

	public void setTimpMediu(double timpMediu) {
		this.timpMediu = timpMediu;
	}

	public void adaugaClient(Client cl) {
		try {
			timpAst += cl.getTimpAsteptare();
			ocupata = true;
			numarClienti++;
			coadaClienti.put(cl);
			timpMediu = timpMediu + cl.getTimpAsteptare();
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void run() {
		while (true) {
			if (this.isOpen == false) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				Client auxCl = coadaClienti.peek();
				if (auxCl != null) {
					try {
						int astT = auxCl.getTimpAsteptare();
						int tmp = auxCl.getTimpAsteptare();
						timpAst += astT;
						for (int i = 0; i < tmp - 1; i++) {
							auxCl.setTimpAsteptare(auxCl.getTimpAsteptare() - 1);
							timpAst--;
							Thread.sleep(1000);
						}
						coadaClienti.remove();
						this.ocupata = false;
						System.out.println('\n' + "Cl: " + auxCl.getNrClient() + " leaves Q: " + (numarCoada + 1));
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else {
					System.out.println('\n' + "Casa " + (numarCoada + 1) + " inchisa!!!");
					this.isOpen = false;
				}
			}
		}
	}
}
